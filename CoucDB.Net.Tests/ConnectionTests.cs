using NUnit.Framework;
using System;
using System.Linq;

namespace CouchDB.Tests
{
    [TestFixture]
    public class ConnectionTests
    {
        public static Connection CreateConnection()
        {
            return new Connection(new Uri("http://localhost:5984"));
        }

        [TestFixtureSetUp]
        public void SetUp()
        {
            var c = CreateConnection();

            c.ListDatabases().Where(x => x.StartsWith("couchbd-dotnet-test-"))
                             .Each(x => c.DeleteDatabase(x));

            c.CreateDatabase("couchbd-dotnet-test-delete-database");
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            var c = CreateConnection();

            c.ListDatabases().Where(x => x.StartsWith("couchbd-dotnet-test-"))
                             .Each(x => c.DeleteDatabase(x));
        }

        [Test]
        public void Connection_can_list_databases()
        {
            Assert.IsNotEmpty(CreateConnection().ListDatabases());
        }

        [Test]
        public void Connection_can_create_database()
        {
            var c = CreateConnection();
            c.CreateDatabase("couchbd-dotnet-test-create-database");
            Assert.IsTrue(c.ListDatabases().Contains("couchbd-dotnet-test-create-database"));
        }

        [Test]
        public void Connection_can_delete_database()
        {
            var c = CreateConnection();
            Assert.IsTrue(c.ListDatabases().Contains("couchbd-dotnet-test-delete-database"));
            c.DeleteDatabase("couchbd-dotnet-test-delete-database");
            Assert.IsFalse(c.ListDatabases().Contains("couchbd-dotnet-test-delete-database"));
        }
    }
}

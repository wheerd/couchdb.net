using NUnit.Framework;
using System;
using System.Diagnostics;
using System.Linq;

namespace CouchDB.Tests
{
    [TestFixture]
    public class DatabaseTests
    {
        public class A : Document
        {
            public string Name;
        }

        public class B
        {
            public string Fubar { get; set; }

            public string[] Tags { get; set; }
        }

        public class C : IHasDocument
        {
            public string Name { get; set; }

            public decimal Cost { get; set; }

            public Document Document
            {
                get;
                set;
            }
        }

        private Connection cn;
        private Database db;
        private Document doc;

        [TestFixtureSetUp]
        public void SetUp()
        {
            Debug.Write("Init");
            cn = new Connection(new Uri("http://localhost:5984"));

            cn.ListDatabases().Where(x => x.StartsWith("couchbd-dotnet-test-"))
                              .Each(x => cn.DeleteDatabase(x));

            cn.CreateDatabase("couchbd-dotnet-test-database");

            db = cn.GetDatabase("couchbd-dotnet-test-database");

            // On different session
            doc = cn.GetDatabase("couchbd-dotnet-test-database").Save(
                new A { Name = "Fnord" },
                new Document { Id = "fnord" }
            );
        }

        [TestFixtureTearDown]
        public void TearDown()
        {
            Debug.Write("Destroy");
            cn.ListDatabases().Where(x => x.StartsWith("couchbd-dotnet-test-"))
                              .Each(x => cn.DeleteDatabase(x));
        }

        [Test]
        public void Database_can_list_documents()
        {
            var docs = db.ListDocuments();

            Assert.IsNotEmpty(docs);
        }

        [Test]
        public void Database_can_load_entity()
        {
            Document d = null;
            var a = db.Load<A>(doc.Id, ref d);

            Assert.IsNotNull(a);
            Assert.AreEqual(a.Id, doc.Id);
            Assert.AreEqual(a.Revision, doc.Revision);
            Assert.AreEqual(a.Name, "Fnord");
        }

        [Test]
        public void Can_create_entity()
        {
            var b = new B { Fubar = "bla", Tags = new[] { "A", "B" } };
            var doc = db.Save(b, new Document { Id = "bla-create" });

            Assert.IsTrue(db.ListDocuments().Any(x => x.Id == doc.Id && x.Revision == doc.Revision));
        }

        [Test]
        public void Can_delete_entity()
        {
            var b = new B { Fubar = "bla", Tags = new[] { "A", "B" } };
            var doc = db.Save(b, new Document { Id = "bla-delete" });
            db.Delete(b, doc);

            Assert.IsFalse(db.ListDocuments().Any(x => x.Id == doc.Id && x.Revision == doc.Revision));
        }

        [Test]
        public void Can_update_entity_after_creating_it()
        {
            var b = new B { Fubar = "bla", Tags = new[] { "A", "B" } };
            var doc = db.Save(b, new Document { Id = "bla-update" });
            var doc2 = db.Save(b, doc);

            Assert.AreEqual(doc.Id, doc2.Id);
            Assert.AreNotEqual(doc.Revision, doc2.Revision);
        }
    }
}

using Newtonsoft.Json;
using System;

namespace CouchDB
{
    public interface IHasDocument
    {
        [JsonIgnore]
        Document Document { get; set; }
    }

    public class Document
    {
        [JsonIgnore]
        public Database Database { get; set; }

        [JsonIgnore]
        public string Id { get; set; }

        [JsonIgnore]
        public Uri Location
        {
            get
            {
                var path = String.IsNullOrEmpty(Revision) ? Id : Id + "?rev=" + Revision;

                return Database != null ? new Uri(Database.Location, path) : null;
            }
        }

        [JsonIgnore]
        public string Revision { get; set; }

        public static string IdFor<TEntity>(string id = null)
        {
            return String.Format("{0}-{1}", typeof(TEntity).Name.ToLowerInvariant(), id ?? Guid.NewGuid().ToString());
        }
    }
}

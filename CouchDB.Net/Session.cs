using System;
using System.Collections.Generic;
using System.Linq;

namespace CouchDB
{
    public class Session
    {
        private readonly Dictionary<object, Document> _entities = new Dictionary<object, Document>();

        public Database Database { get; private set; }

        public Session(Database database)
        {
            Database = database;
        }

        public Document GetDocument<TEntity>(TEntity entity)
            where TEntity : class
        {
            lock (_entities)
            {
                return _entities.ContainsKey(entity) ? _entities[entity] : null;
            }
        }

        public void Attach<TEntity>(TEntity entity, Document doc)
            where TEntity : class
        {
            lock (_entities)
            {
                if (IsAttached(doc.Id))
                {
                    throw new Exception("An entity with this key is already enrolled.");
                }

                _entities.Add(entity, doc);
            }
        }

        public bool IsAttached(string id)
        {
            lock (_entities)
            {
                return _entities.Any(kv => kv.Value.Id == id);
            }
        }

        public bool IsAttached<TEntity>(TEntity entity)
        {
            lock (_entities)
            {
                return _entities.ContainsKey(entity);
            }
        }

        public TEntity Load<TEntity>(string id)
            where TEntity : class
        {
            TEntity entity;

            lock (_entities)
            {
                entity = _entities.Where(kv => kv.Value.Id == id).Select(kv => (TEntity)kv.Key).FirstOrDefault();

                if (entity != null)
                {
                    return entity;
                }
            }

            Document doc = null;
            entity = Database.Load<TEntity>(id, ref doc);

            lock (_entities)
            {
                _entities[entity] = doc;
            }

            return entity;
        }

        public Document Save<TEntity>(TEntity entity, string id)
            where TEntity : class
        {
            var doc = GetDocument(entity);
            if (doc != null)
            {
                if (doc.Id != id)
                {
                    throw new Exception(String.Format("This entity is already saved under the id '{0}' and cannot be saves as '{1}'.", doc.Id, id));
                }

                doc = Database.Save(entity, doc);
            }
            else
            {
                doc = Database.Save(entity, new Document { Database = Database, Id = id });
            }

            UpdateEntityDocument(entity, doc);

            return doc;
        }

        public Document Save<TEntity>(TEntity entity)
            where TEntity : class
        {
            var doc = GetDocument(entity) ?? Database.GetDocument(entity) ?? new Document();

            if (String.IsNullOrEmpty(doc.Id))
            {
                doc.Id = Document.IdFor<TEntity>();
                doc.Revision = null;
            }

            doc = Database.Save(entity, doc);

            UpdateEntityDocument(entity, doc);

            return doc;
        }

        private void UpdateEntityDocument<TEntity>(TEntity entity, Document doc)
            where TEntity : class
        {
            lock (_entities)
            {
                if (_entities.ContainsKey(entity))
                {
                    _entities.Remove(entity);
                }
                _entities.Add(entity, doc);
            }
        }

        public void Delete<TEntity>(TEntity entity)
            where TEntity : class
        {
            var doc = GetDocument(entity);

            if (doc == null)
            {
                throw new IndexOutOfRangeException("The entity is not attached to this session.");
            }

            Database.Delete(entity, doc);

            lock (_entities)
            {
                _entities.Remove(entity);
            }
        }
    }
}

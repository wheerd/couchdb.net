using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace CouchDB
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> Each<T>(this IEnumerable<T> e, Action<T> a)
        {
            foreach (var x in e) a(x);
            return e;
        }
    }

    public static class JsonExtensions
    {
        public static T Deserialize<T>(this JsonSerializer serializer, JsonReader reader)
        {
            return (T)serializer.Deserialize(reader, typeof(T));
        }

        public static TEntity ToEntity<TEntity>(this JToken token, ref Document doc)
        {
            doc.Id = token.Value<string>("_id");
            doc.Revision = token.Value<string>("_rev");

            var entity = new JsonSerializer().Deserialize<TEntity>(new JTokenReader(token));

            var document = entity as Document;
            if (document != null)
            {
                document.Id = doc.Id;
                document.Revision = doc.Revision;
                document.Database = doc.Database;
                doc = document;
            }

            var withdocument = entity as IHasDocument;
            if (withdocument != null)
            {
                withdocument.Document = doc;
            }

            return entity;
        }
    }

    public class Connection
    {
        private string[] _databases;

        public Connection(Uri location, ICredentials credentials = null)
        {
            Location = location;
            Credentials = credentials;

            if (credentials == null && !String.IsNullOrEmpty(location.UserInfo))
            {
                var info = location.UserInfo.Split(':');
                Credentials = new NetworkCredential(info[0], info[1]);
            }
        }

        public ICredentials Credentials { get; private set; }

        public Uri Location { get; private set; }

        public Database GetDatabase(string name)
        {
            return new Database(this, name);
        }

        #region Requests

        public T DataRequest<T>(string path, string method = "GET", object data = null, string content = null)
        {
            using (var reader = Request(path, method, data, content).GetCouchResponse())
            {
                return new JsonSerializer().Deserialize<T>(reader);
            }
        }

        public T DataRequest<T>(Uri uri, string method = "GET", object data = null, string content = null)
        {
            using (var reader = Request(uri, method, data, content).GetCouchResponse())
            {
                return new JsonSerializer().Deserialize<T>(reader);
            }
        }

        public T DocumentRequest<T>(ref Document doc, string method = "GET", object data = null, string content = null)
        {
            using (var reader = Request(doc.Location, method, data, content).GetCouchResponse())
            {
                return JToken.ReadFrom(reader).ToEntity<T>(ref doc);
            }
        }

        public void DoRequest(string path, string method = "GET", object data = null, string content = null)
        {
            Request(path, method, data, content).GetCouchResponse().Close();
        }

        public void DoRequest(Uri uri, string method = "GET", object data = null, string content = null)
        {
            Request(uri, method, data, content).GetCouchResponse().Close();
        }

        public JsonReader JsonRequest(string path, string method = "GET", object data = null, string content = null)
        {
            return Request(path, method, data, content).GetCouchResponse();
        }

        public JsonReader JsonRequest(Uri uri, string method = "GET", object data = null, string content = null)
        {
            return Request(uri, method, data, content).GetCouchResponse();
        }

        public HttpWebRequest Request(Uri uri, string method = "GET", object data = null, string content = null)
        {
            var request = (HttpWebRequest)HttpWebRequest.Create(uri);
            request.Method = method;
            request.Credentials = Credentials;

            if (data != null)
            {
                var token = JToken.FromObject(data, new JsonSerializer());

                var document = data as Document;

                if (document == null && data is IHasDocument)
                {
                    document = ((IHasDocument)data).Document;
                }

                if (document != null)
                {
                    if (!String.IsNullOrEmpty(document.Id))
                    {
                        token["_id"] = document.Id;
                    }

                    if (!String.IsNullOrEmpty(document.Revision))
                    {
                        token["_rev"] = document.Revision;
                    }
                }

                using (var writer = new JsonTextWriter(new StreamWriter(request.GetRequestStream())))
                {
                    token.WriteTo(writer);
                }
            }
            else if (content != null)
            {
                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    writer.Write(content);
                }
            }

            return request;
        }

        public HttpWebRequest Request(string path, string method = "GET", object data = null, string content = null)
        {
            return Request(new Uri(Location, path), method, data, content);
        }
        #endregion Requests

        #region Databases

        public void CreateDatabase(string database)
        {
            var request = Request(GetDatabaseUri(database), "PUT");
            request.GetCouchResponse().Close();

            _databases = null;
        }

        public void DeleteDatabase(string database)
        {
            var request = Request(GetDatabaseUri(database), "DELETE");
            request.GetCouchResponse().Close();

            _databases = null;
        }

        public Uri GetDatabaseUri(string database)
        {
            return new Uri(Location, GetSafeDatabaseName(database, true));
        }

        public string GetSafeDatabaseName(string database, bool trailingSlash = false)
        {
            InvalidDatabaseNameException.Validate(database);

            var location = new Uri(Location, database);
            var relative = Location.MakeRelativeUri(location).OriginalString;

            if (!trailingSlash && relative.EndsWith("/"))
            {
                return relative.Substring(0, relative.Length - 1).Replace("/", "%2F");
            }
            else if (trailingSlash && !relative.EndsWith("/"))
            {
                return relative.Replace("/", "%2F") + "/";
            }

            return relative;
        }

        public string[] ListDatabases(bool cached = false)
        {
            if (cached && _databases != null)
            {
                return _databases;
            }

            _databases = DataRequest<string[]>("_all_dbs");

            return _databases;
        }
        #endregion Databases

        #region Config

        public string GetConfig(string section, string name)
        {
            return DataRequest<string>(String.Format("_config/{0}/{1}", section, name), method: "GET");
        }

        public Dictionary<string, string> GetConfigSection(string section)
        {
            return DataRequest<Dictionary<string, string>>(String.Format("_config/{0}", section), method: "GET");
        }

        public void RemoveConfig(string section, string name)
        {
            DoRequest(String.Format("_config/{0}/{1}", section, name), "DELETE");
        }

        public void SetConfig(string section, string name, string value)
        {
            DoRequest(String.Format("_config/{0}/{1}", section, name), method: "PUT", data: value);
        }
        #endregion Config

        #region Admins

        public void AddAdmin(string name, string password)
        {
            SetConfig("admins", name, password);
        }

        public string[] GetAdmins()
        {
            return GetConfigSection("admins").Select(a => a.Key).ToArray();
        }

        public void RemoveAdmin(string name)
        {
            RemoveConfig("admins", name);
        }
        #endregion Admins
    }

    internal static class __HttpWebRequestExtensions
    {
        public static JsonReader GetCouchResponse(this HttpWebRequest request)
        {
            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                return new JsonTextReader(new StreamReader(response.GetResponseStream()));
            }
            catch (WebException e)
            {
                if (e.Response != null)
                {
                    using (var reader = new StreamReader(e.Response.GetResponseStream()))
                    {
                        var error = JsonConvert.DeserializeObject<__CouchError>(reader.ReadToEnd());

                        throw CouchException.CreateException((HttpWebResponse)e.Response, error.error, error.reason);
                    }
                }

                throw;
            }
        }

        private class __CouchError
        {
            public string error { get; set; }

            public string reason { get; set; }
        }
    }
}

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace CouchDB
{
    public class Query<TEntity> : IEnumerable<TEntity>
        where TEntity : class
    {
        public Session Session { get; private set; }

        public Database Database
        {
            get { return _database ?? Session.Database; }
            private set { _database = value; }
        }

        private Database _database;

        public string Design { get; private set; }

        public string View { get; private set; }

        private JToken _key;

        public JArray _keys;

        private JToken _startkey;

        private string _startkey_docid;

        private JToken _endkey;

        private string _endkey_docid;

        private int? _limit;

        private int? _skip;

        private bool _descending = false;

        private bool? _reduce;

        private bool _group = false;

        private int? _group_level;

        private bool _include_docs = false;

        private bool _inclusive_end = true;

        private bool _update_seq = false;

        public Query(Database database, string design, string view, bool group = false)
        {
            Database = database;
            Design = design;
            View = view;
            _group = group;
        }

        public Query(Session session, string design, string view, bool group = false)
        {
            Session = session;
            Design = design;
            View = view;
            _group = group;
        }

        public class Result
        {
            public class Row
            {
                [JsonProperty("id")]
                public string Id { get; set; }

                [JsonProperty("key")]
                public JToken Key { get; set; }

                [JsonProperty("value")]
                public JToken Value { get; set; }

                [JsonProperty("doc")]
                public JToken Document { get; set; }

                public Session Session { get; set; }

                public Database Database { get; set; }

                private TEntity _entity;

                public TEntity Entity
                {
                    get
                    {
                        if (String.IsNullOrEmpty(Id))
                        {
                            return null;
                        }

                        if (_entity != null)
                        {
                            return _entity;
                        }

                        if (Document != null)
                        {
                            var doc = new Document
                            {
                                Id = Document.Value<string>("_id"),
                                Revision = Document.Value<string>("_rev"),
                                Database = Database
                            };

                            if (Session != null)
                            {
                                if (Session.IsAttached(doc.Id))
                                {
                                    _entity = Session.Load<TEntity>(doc.Id);
                                }
                                else
                                {
                                    _entity = Document.ToEntity<TEntity>(ref doc);
                                    Session.Attach(_entity, doc);
                                }
                            }
                            else
                            {
                                _entity = Document.ToEntity<TEntity>(ref doc);
                            }
                        }
                        else if (Session != null)
                        {
                            _entity = Session.Load<TEntity>(Id);
                        }
                        else if (Database != null)
                        {
                            Document t = null;
                            _entity = Database.Load<TEntity>(Id, ref t);
                        }

                        return _entity;
                    }
                }
            }

            [JsonProperty("total_rows")]
            public long Total { get; set; }

            [JsonProperty("offset")]
            public long Offset { get; set; }

            [JsonProperty("rows")]
            public Row[] Rows { get; set; }

            internal Result WithDatabase(Session sx, Database db)
            {
                foreach (var row in Rows)
                {
                    row.Session = sx;
                    row.Database = db;
                }

                return this;
            }
        }

        public Result Execute()
        {
            return _cached = Database.Connection.DataRequest<Result>(Location).WithDatabase(Session, Database);
        }

        private Result _cached;

        #region Query Methods

        public Query<TEntity> From(object key)
        {
            _cached = null;
            _startkey = JToken.FromObject(key);
            return this;
        }

        public Query<TEntity> FromDoc(string id)
        {
            _cached = null;
            _startkey_docid = id;
            return this;
        }

        public Query<TEntity> To(object key)
        {
            _cached = null;
            _endkey = JToken.FromObject(key);
            return this;
        }

        public Query<TEntity> ToDoc(string id)
        {
            _cached = null;
            _endkey_docid = id;
            return this;
        }

        public Query<TEntity> Exactly(object key)
        {
            _cached = null;
            _key = JToken.FromObject(key);
            return this;
        }

        public Query<TEntity> WithKeys(params object[] keys)
        {
            _cached = null;
            _keys = JArray.FromObject(keys);
            return this;
        }

        public Query<TEntity> Limit(int limit)
        {
            _cached = null;
            _limit = limit;
            return this;
        }

        public Query<TEntity> Skip(int skip)
        {
            _cached = null;
            _skip = skip;
            return this;
        }

        public Query<TEntity> Descending(bool descending = true)
        {
            _cached = null;
            _descending = descending;
            return this;
        }

        public Query<TEntity> Reduce(bool reduce = true)
        {
            _cached = null;
            _reduce = reduce;
            return this;
        }

        public Query<TEntity> Group(int level = 999)
        {
            _cached = null;

            if (level == 999)
            {
                _group = true;
                _group_level = null;
            }
            else if (level == 0)
            {
                _group = false;
                _group_level = null;
            }
            else
            {
                _group = false;
                _group_level = level;
            }

            return this;
        }

        public Query<TEntity> WithDocuments()
        {
            _cached = null;
            _include_docs = true;
            return this;
        }

        public Query<TEntity> WithEnd()
        {
            _cached = null;
            _inclusive_end = true;
            return this;
        }

        public Query<TEntity> WithUpdateSeq()
        {
            _cached = null;
            _update_seq = true;
            return this;
        }

        #endregion Query Methods

        public IEnumerator<TEntity> GetEnumerator()
        {
            return (_cached ?? Execute()).Rows.Select(r => r.Entity).GetEnumerator();
        }

        public NameValueCollection Parameters
        {
            get
            {
                var p = new NameValueCollection();

                if (_key != null) p.Add("key", _key.ToString(Formatting.None));
                if (_keys != null) p.Add("keys", _keys.ToString(Formatting.None));
                if (_startkey != null) p.Add("startkey", _startkey.ToString(Formatting.None));
                if (_startkey_docid != null) p.Add("startkey_docid", JsonConvert.SerializeObject(_startkey_docid));
                if (_endkey != null) p.Add("endkey", _endkey.ToString(Formatting.None));
                if (_endkey_docid != null) p.Add("endkey_docid", JsonConvert.SerializeObject(_endkey_docid));
                if (_limit != null) p.Add("limit", _limit.ToString());
                if (_skip != null) p.Add("skip", _skip.ToString());
                if (_descending) p.Add("descending", "true");
                if (_reduce != null) p.Add("reduce", _reduce.ToString());
                if (_group) p.Add("group", "true");
                if (_group_level != null) p.Add("group_level", _group_level.ToString());
                if (_include_docs) p.Add("include_docs", "true");
                if (!_inclusive_end) p.Add("inclusive_end", "false");
                if (_update_seq) p.Add("update_seq", "true");

                return p;
            }
        }

        public virtual Uri Location
        {
            get
            {
                var path = String.Format("_design/{0}/_view/{1}?{2}", Design, View, Parameters.ToQuery());

                return new Uri(Database.Location, path);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public static class NameValueCollectionExtensions
    {
        public static string ToQuery(this NameValueCollection nvc)
        {
            return string.Join("&",
                    Array.ConvertAll(nvc.AllKeys, key => string.Format(
                        "{0}={1}",
                        HttpUtility.UrlEncode(key),
                        HttpUtility.UrlEncode(nvc[key])
                    )
                )
            );
        }
    }

    internal class AllDocumentsQuery : Query<JToken>
    {
        public AllDocumentsQuery(Session sx)
            : base(sx, null, null, false)
        {
        }

        public AllDocumentsQuery(Database db)
            : base(db, null, null, false)
        {
        }

        public override Uri Location
        {
            get
            {
                return new Uri(Database.Location, "_all_docs");
            }
        }
    }
}

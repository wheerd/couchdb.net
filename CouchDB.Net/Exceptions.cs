using System;
using System.Linq;
using System.Net;

namespace CouchDB
{
    public class CouchException : Exception
    {
        public CouchException(string url, int status, string error, string reason)
            : base(error)
        {
            Url = url;
            Status = status;
            Reason = reason;
        }

        public string Reason { get; private set; }

        public int Status { get; private set; }

        public string Url { get; private set; }

        public static CouchException CreateException(string url, int status, string error, string reason)
        {
            if (status == 404)
            {
                return new CouchException(url, status, "The resource could not be found (404).", reason);
            }
            return new CouchException(url, status, error, reason);
        }

        public static CouchException CreateException(HttpWebResponse response, string error, string reason)
        {
            return CreateException(response.ResponseUri.ToString(), (int)response.StatusCode, error, reason);
        }
    }

    public class InvalidDatabaseNameException : Exception
    {
        private static char[] ValidSymbols = new[] { '_', '$', '(', ')', '+', '-', '/' };

        private InvalidDatabaseNameException(string database, string message)
            : base(message)
        {
            Database = database;
        }

        public string Database { get; private set; }

        public static void Validate(string database)
        {
            if ("_replicator" == database || "_users" == database)
                return;

            if (null == database)
            {
                throw new ArgumentNullException("database", "Database name argument must not be null.");
            }
            if (string.Empty == database)
            {
                throw new InvalidDatabaseNameException(database, "Database name must not be empty.");
            }
            if (database.Any(x => char.IsUpper(x)))
            {
                throw new InvalidDatabaseNameException(database, "Database name must not contain uppercase letters (http://wiki.apache.org/couchdb/HTTP_database_API).");
            }
            if (!char.IsLetter(database[0]))
            {
                throw new InvalidDatabaseNameException(database, "Database name must begin with a lowercase letter (http://wiki.apache.org/couchdb/HTTP_database_API).");
            }
            if (database.Any(x => !char.IsLetterOrDigit(x) && !ValidSymbols.Contains(x)))
            {
                throw new InvalidDatabaseNameException(database, "Database name contains invalid symbols (http://wiki.apache.org/couchdb/HTTP_database_API).");
            }
        }
    }
}

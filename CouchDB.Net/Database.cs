using System;
using System.Collections.Generic;
using System.Linq;

namespace CouchDB
{
    public class Database
    {
        public Database(Connection connection, string database)
        {
            InvalidDatabaseNameException.Validate(database);

            Connection = connection;
            Name = database;
            Location = connection.GetDatabaseUri(database);
        }

        public Connection Connection { get; private set; }

        public Uri Location { get; private set; }

        public string Name { get; private set; }

        public static Document GetDocument<TEntity>(TEntity entity)
            where TEntity : class
        {
            return (entity as Document)
                    ?? (entity is IHasDocument
                        ? ((IHasDocument)entity).Document
                        : null);
        }

        public IEnumerable<Document> ListDocuments()
        {
            var result = new AllDocumentsQuery(this).Execute();

            return result.Rows.Select(r => new Document
                {
                    Id = r.Id,
                    Revision = r.Value.Value<string>("rev")
                }).ToList();
        }

        public TEntity Load<TEntity>(string id, ref Document doc)
            where TEntity : class
        {
            doc = new Document
            {
                Database = this,
                Id = id
            };

            return Connection.DocumentRequest<TEntity>(ref doc);
        }

        public Document Save<TEntity>(TEntity entity, Document doc)
            where TEntity : class
        {
            if (doc == null)
            {
                doc = new Document();
            }

            if (doc.Database == null)
            {
                doc.Database = this;
            }

            if (String.IsNullOrEmpty(doc.Id))
            {
                doc.Id = Document.IdFor<TEntity>();
                doc.Revision = null;
            }

            var response = Connection.DataRequest<DocumentResponse>(doc.Location, method: "PUT", data: entity);

            if (!response.ok)
            {
                throw new Exception("Document could not be saved");
            }

            doc = UpdateEntityDocument(entity, response.ToDocument(this));

            return doc;
        }

        private Document UpdateEntityDocument<TEntity>(TEntity entity, Document doc)
            where TEntity : class
        {
            var document = entity as Document;
            if (document != null)
            {
                document.Id = doc.Id;
                document.Revision = doc.Revision;
                document.Database = doc.Database;
                doc = document;
            }

            var ihasdoc = entity as IHasDocument;
            if (ihasdoc != null)
            {
                ihasdoc.Document = doc;
            }

            return doc;
        }

        public void Delete<TEntity>(TEntity entity, Document doc = null)
            where TEntity : class
        {
            doc = doc ?? GetDocument(entity);

            if (doc == null)
            {
                throw new IndexOutOfRangeException("The entity does not contain an id.");
            }

            var response = Connection.DataRequest<DocumentResponse>(doc.Location, "DELETE");

            if (!response.ok)
            {
                throw new Exception("Could not delete the entity");
            }
        }

        private class DocumentResponse
        {
            public string id { get; set; }

            public bool ok { get; set; }

            public string rev { get; set; }

            public Document ToDocument(Database database)
            {
                return new Document
                {
                    Database = database,
                    Id = id,
                    Revision = rev
                };
            }
        }
    }
}
